package com.rapidtech.springdasar.singleton;

public class Car {

    private static Car car;

    public static Car getInstance(){
        if (car == null){
            car = new Car();
        }
        return car;
    }

    private Car(){

    }
}
