package com.rapidtech.springdasar.singleton;

public class Student {
    private static Student student;

    public static Student getInstance(){
        if (student == null){
            student = new Student();
        }
        return student;
    }

    private Student(){

    }
}
