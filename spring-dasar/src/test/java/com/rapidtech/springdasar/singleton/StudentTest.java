package com.rapidtech.springdasar.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    void singletonTest() {
        Student student1 = Student.getInstance();
        Student student2 = Student.getInstance();
        Student student3 = Student.getInstance();

        Assertions.assertSame(student1, student2);
        Assertions.assertSame(student1, student3);
        Assertions.assertSame(student2, student3);
    }
}