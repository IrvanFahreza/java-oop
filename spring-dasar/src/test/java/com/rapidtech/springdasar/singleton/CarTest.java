package com.rapidtech.springdasar.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {

    @Test
    void singletonTest() {
        Car car1 = Car.getInstance();
        Car car2 = Car.getInstance();
        Car car3 = Car.getInstance();

        Assertions.assertSame(car1, car2);
        Assertions.assertSame(car1, car3);
    }
}