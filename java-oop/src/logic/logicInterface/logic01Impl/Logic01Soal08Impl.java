package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal08Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal08Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        char huruf = 'A';
        int angka = 2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0) {
                this.logic.array[0][i] = String.valueOf(huruf);
            } else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(angka);
                angka+=2;
            }
            huruf++;
        }
    }
    public void isiArray2(){
        char word = 'A';
        int number = 2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0) {
                this.logic.array[0][i] = String.valueOf(word);
            } else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(number);
                number+=2;
            }
            word++;
        }
    }
    public void isiArray3(){
        char alphabets = 'A';
        int numb = 2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0) {
                this.logic.array[0][i] = String.valueOf(alphabets);
            } else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(numb);
                numb+=2;
            }
            alphabets++;
        }
    }
    public void isiArray4(){
        char x = 'A';
        int y = 2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0) {
                this.logic.array[0][i] = String.valueOf(x);
            } else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(y);
                y+=2;
            }
            x++;
        }
    }
    public void isiArray5(){
        char a = 'A';
        int b = 2;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0) {
                this.logic.array[0][i] = String.valueOf(a);
            } else if(i%2==1){
                this.logic.array[0][i] = String.valueOf(b);
                b+=2;
            }
            a++;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
