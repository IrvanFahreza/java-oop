package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal05Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal05Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int[][] deret = new int [logic.n][logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i==0 || i ==1 | i == 2) {
                deret[0][i] = 1;
            } else {
                deret[0][i] = deret[0][i-1] + deret[0][i-2] + deret[0][i-3];
            }
            this.logic.array[0][i] = String.valueOf(deret[0][i]);
        }
    }
    public void isiArray2(){
        int[][] a = new int [logic.n][logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i==0 || i ==1 | i == 2) {
                a[0][i] = 1;
            } else {
                a[0][i] = a[0][i-1] + a[0][i-2] + a[0][i-3];
            }
            this.logic.array[0][i] = String.valueOf(a[0][i]);
        }
    }
    public void isiArray3(){
        int[][] b = new int [logic.n][logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i==0 || i ==1 | i == 2) {
                b[0][i] = 1;
            } else {
                b[0][i] = b[0][i-1] + b[0][i-2] + b[0][i-3];
            }
            this.logic.array[0][i] = String.valueOf(b[0][i]);
        }
    }
    public void isiArray4(){
        int[][] c = new int [logic.n][logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i==0 || i ==1 | i == 2) {
                c[0][i] = 1;
            } else {
                c[0][i] = c[0][i-1] + c[0][i-2] + c[0][i-3];
            }
            this.logic.array[0][i] = String.valueOf(c[0][i]);
        }
    }
    public void isiArray5(){
        int[][] d = new int [logic.n][logic.n];
        for (int i = 0; i < this.logic.n; i++) {
            if (i==0 || i ==1 | i == 2) {
                d[0][i] = 1;
            } else {
                d[0][i] = d[0][i-1] + d[0][i-2] + d[0][i-3];
            }
            this.logic.array[0][i] = String.valueOf(d[0][i]);
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
