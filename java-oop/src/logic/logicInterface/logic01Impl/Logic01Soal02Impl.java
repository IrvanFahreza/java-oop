package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal02Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal02Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int a=1;
        int b=3;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0){
                this.logic.array[0][i] = String.valueOf(a);
                a+=1;
            } else if (i%2==1){
                this.logic.array[0][i] = String.valueOf(b);
                b+=3;
            }
        }
    }

    public void isiArray2(){
        int c=1;
        int d=3;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0){
                this.logic.array[0][i] = String.valueOf(c);
                c+=1;
            } else if (i%2==1){
                this.logic.array[0][i] = String.valueOf(d);
                d+=3;
            }
        }
    }

    public void isiArray3(){
        int x=1;
        int y=3;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0){
                this.logic.array[0][i] = String.valueOf(x);
                x+=1;
            } else if (i%2==1){
                this.logic.array[0][i] = String.valueOf(y);
                y+=3;
            }
        }
    }

    public void isiArray4(){
        int m=1;
        int n=3;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0){
                this.logic.array[0][i] = String.valueOf(m);
                m+=1;
            } else if (i%2==1){
                this.logic.array[0][i] = String.valueOf(n);
                n+=3;
            }
        }
    }

    public void isiArray5(){
        int o=1;
        int p=3;
        for (int i = 0; i < this.logic.n; i++) {
            if (i%2==0){
                this.logic.array[0][i] = String.valueOf(o);
                o+=1;
            } else if (i%2==1){
                this.logic.array[0][i] = String.valueOf(p);
                p+=3;
            }
        }
    }


    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
