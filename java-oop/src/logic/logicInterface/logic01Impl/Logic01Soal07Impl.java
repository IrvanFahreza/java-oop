package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal07Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal07Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        char huruf = 'A';
        for (int i = 0; i< this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(huruf);
            huruf++;
        }
    }
    public void isiArray2(){
        char word = 'A';
        for (int i = 0; i< this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(word);
            word++;
        }
    }
    public void isiArray3(){
        char alphabets = 'A';
        for (int i = 0; i< this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(alphabets);
            alphabets++;
        }
    }
    public void isiArray4(){
        char x = 'A';
        for (int i = 0; i< this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(x);
            x++;
        }
    }
    public void isiArray5(){
        char y = 'A';
        for (int i = 0; i< this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(y);
            y++;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
