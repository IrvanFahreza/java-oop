package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal03Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal03Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int angka = 0;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(angka);
            angka+=2;
        }
    }
    public void isiArray2(){
        int n = 0;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(n);
            n+=2;
        }
    }
    public void isiArray3(){
        int numb = 0;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(numb);
            numb+=2;
        }
    }
    public void isiArray4(){
        int x = 0;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(x);
            x+=2;
        }
    }
    public void isiArray5(){
        int y = 0;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(y);
            y+=2;
        }
    }


    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }

}
