package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal01Impl implements LogicInterface {
    private final BasicLogic logic;
    public Logic01Soal01Impl(BasicLogic logic) {
        this.logic=logic;
    }

    public void isiArray(){
        int angka = 1;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(angka);
            angka+=3;
        }
    }

    public void isiArray2(){
        int numb = 1;
        for (int a = 0; a < this.logic.n; a++) {
            this.logic.array[0][a] = String.valueOf(numb);
            numb+=3;
        }
    }

    public void isiArray3(){
        int n = 1;
        for (int b = 0; b < this.logic.n; b++) {
            this.logic.array[0][b] = String.valueOf(n);
            n+=3;
        }
    }

    public void isiArray4(){
        int x = 1;
        for (int c = 0; c < this.logic.n; c++) {
            this.logic.array[0][c] = String.valueOf(x);
            x+=3;
        }
    }

    public void isiArray5(){
        int y = 1;
        for (int d = 0; d < this.logic.n; d++) {
            this.logic.array[0][d] = String.valueOf(y);
            y+=3;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }

    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }

    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }

    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }

    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
