package logic.logicInterface.logic01Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic01Soal09Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal09Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf((int)Math.pow(3,i));
        }
    }
    public void isiArray2(){
        for (int j = 0; j < this.logic.n; j++) {
            this.logic.array[0][j] = String.valueOf((int)Math.pow(3,j));
        }
    }
    public void isiArray3(){
        for (int k = 0; k < this.logic.n; k++) {
            this.logic.array[0][k] = String.valueOf((int)Math.pow(3,k));
        }
    }
    public void isiArray4(){
        for (int l = 0; l < this.logic.n; l++) {
            this.logic.array[0][l] = String.valueOf((int)Math.pow(3,l));
        }
    }
    public void isiArray5(){
        for (int m = 0; m < this.logic.n; m++) {
            this.logic.array[0][m] = String.valueOf((int)Math.pow(3,m));
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.printSingle();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.printSingle();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.printSingle();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.printSingle();
    }
}
