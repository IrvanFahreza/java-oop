package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;


public class Logic02Soal04Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal04Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int[][] deret = new int[logic.n][logic.n];
        for (int i = 0; i < deret.length; i++) {
            int angka = 1;
            int akhir = 34;
            int tengah = this.logic.n/2+1;
            for (int j = 0; j < deret.length; j++) {
                if( ((i+j==0 || i+j<=1) && i==0) ||
                        ((i+j==this.logic.n-1 || i+j<=this.logic.n+1) && i==this.logic.n-1) ||
                        ((i+j == tengah-1 || i+j == tengah) && i==4)) {
                    deret[i][j] = Integer.parseInt(String.valueOf(angka));
                } else if (j==0){
                    deret[i][j] = Integer.parseInt(String.valueOf(angka));
                } else if (j==4){
                    deret[i][j] = Integer.parseInt(String.valueOf(tengah));
                } else if(i<=j && j==8){
                    deret[i][j] = Integer.parseInt(String.valueOf(akhir));
                }
                else if(i==0 || i==8 || i==4) {
                    deret[i][j] = deret[i][j - 1] + deret[i][j - 2];
                }
                this.logic.array[i][j] = String.valueOf(deret[i][j]);
            }
        }
    }
    public void isiArray2(){
        int[][] baris = new int[logic.n][logic.n];
        for (int i = 0; i < baris.length; i++) {
            int a = 1;
            int b = 34;
            int c = this.logic.n/2+1;
            for (int j = 0; j < baris.length; j++) {
                if( ((i+j==0 || i+j<=1) && i==0) ||
                        ((i+j==this.logic.n-1 || i+j<=this.logic.n+1) && i==this.logic.n-1) ||
                        ((i+j == c-1 || i+j == c) && i==4)) {
                    baris[i][j] = Integer.parseInt(String.valueOf(a));
                } else if (j==0){
                    baris[i][j] = Integer.parseInt(String.valueOf(a));
                } else if (j==4){
                    baris[i][j] = Integer.parseInt(String.valueOf(c));
                } else if(i<=j && j==8){
                    baris[i][j] = Integer.parseInt(String.valueOf(b));
                }
                else if(i==0 || i==8 || i==4) {
                    baris[i][j] = baris[i][j - 1] + baris[i][j - 2];
                }
                this.logic.array[i][j] = String.valueOf(baris[i][j]);

            }
        }
    }
    public void isiArray3(){
        int[][] urutan = new int[logic.n][logic.n];
        for (int i = 0; i < urutan.length; i++) {
            int x = 1;
            int y = 34;
            int z = this.logic.n/2+1;
            for (int j = 0; j < urutan.length; j++) {
                if( ((i+j==0 || i+j<=1) && i==0) ||
                        ((i+j==this.logic.n-1 || i+j<=this.logic.n+1) && i==this.logic.n-1) ||
                        ((i+j == z-1 || i+j == z) && i==4)) {
                    urutan[i][j] = Integer.parseInt(String.valueOf(x));
                } else if (j==0){
                    urutan[i][j] = Integer.parseInt(String.valueOf(x));
                } else if (j==4){
                    urutan[i][j] = Integer.parseInt(String.valueOf(z));
                } else if(i<=j && j==8){
                    urutan[i][j] = Integer.parseInt(String.valueOf(y));
                }
                else if(i==0 || i==8 || i==4) {
                    urutan[i][j] = urutan[i][j - 1] + urutan[i][j - 2];
                }
                this.logic.array[i][j] = String.valueOf(urutan[i][j]);

            }
        }
    }
    public void isiArray4(){
        int[][] array = new int[logic.n][logic.n];
        for (int i = 0; i < array.length; i++) {
            int p = 1;
            int q = 34;
            int r = this.logic.n/2+1;
            for (int j = 0; j < array.length; j++) {
                if( ((i+j==0 || i+j<=1) && i==0) ||
                        ((i+j==this.logic.n-1 || i+j<=this.logic.n+1) && i==this.logic.n-1) ||
                        ((i+j == r-1 || i+j == r) && i==4)) {
                    array[i][j] = Integer.parseInt(String.valueOf(p));
                } else if (j==0){
                    array[i][j] = Integer.parseInt(String.valueOf(p));
                } else if (j==4){
                    array[i][j] = Integer.parseInt(String.valueOf(r));
                } else if(i<=j && j==8){
                    array[i][j] = Integer.parseInt(String.valueOf(q));
                }
                else if(i==0 || i==8 || i==4) {
                    array[i][j] = array[i][j - 1] + array[i][j - 2];
                }
                this.logic.array[i][j] = String.valueOf(array[i][j]);

            }
        }
    }
    public void isiArray5(){
        int[][] row = new int[logic.n][logic.n];
        for (int i = 0; i < row.length; i++) {
            int number = 1;
            int ends = 34;
            int mid = this.logic.n/2+1;
            for (int j = 0; j < row.length; j++) {
                if( ((i+j==0 || i+j<=1) && i==0) ||
                        ((i+j==this.logic.n-1 || i+j<=this.logic.n+1) && i==this.logic.n-1) ||
                        ((i+j == mid-1 || i+j == mid) && i==4)) {
                    row[i][j] = Integer.parseInt(String.valueOf(number));
                } else if (j==0){
                    row[i][j] = Integer.parseInt(String.valueOf(number));
                } else if (j==4){
                    row[i][j] = Integer.parseInt(String.valueOf(mid));
                } else if(i<=j && j==8){
                    row[i][j] = Integer.parseInt(String.valueOf(ends));
                }
                else if(i==0 || i==8 || i==4) {
                    row[i][j] = row[i][j - 1] + row[i][j - 2];
                }
                this.logic.array[i][j] = String.valueOf(row[i][j]);

            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.print();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.print();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.print();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.print();
    }
}
