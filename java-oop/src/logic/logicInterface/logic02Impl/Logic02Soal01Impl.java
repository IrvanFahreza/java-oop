package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal01Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal01Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {
            int angka = 1;
            for (int j = 0; j < this.logic.n; j++) {
                if(i == j || i+j == this.logic.n-1) {
                    this.logic.array[i][j] = String.valueOf(angka);
                }
                angka+=1;
            }
        }
    }
    public void isiArray2(){
        for (int i = 0; i < this.logic.n; i++) {
            int number = 1;
            for (int j = 0; j < this.logic.n; j++) {
                if(i == j || i+j == this.logic.n-1) {
                    this.logic.array[i][j] = String.valueOf(number);
                }
                number+=1;
            }
        }
    }
    public void isiArray3(){
        for (int i = 0; i < this.logic.n; i++) {
            int num = 1;
            for (int j = 0; j < this.logic.n; j++) {
                if(i == j || i+j == this.logic.n-1) {
                    this.logic.array[i][j] = String.valueOf(num);
                }
                num+=1;
            }
        }
    }
    public void isiArray4(){
        for (int i = 0; i < this.logic.n; i++) {
            int x = 1;
            for (int j = 0; j < this.logic.n; j++) {
                if(i == j || i+j == this.logic.n-1) {
                    this.logic.array[i][j] = String.valueOf(x);
                }
                x+=1;
            }
        }
    }
    public void isiArray5(){
        for (int i = 0; i < this.logic.n; i++) {
            int y = 1;
            for (int j = 0; j < this.logic.n; j++) {
                if(i == j || i+j == this.logic.n-1) {
                    this.logic.array[i][j] = String.valueOf(y);
                }
                y+=1;
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.print();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.print();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.print();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.print();
    }
}
