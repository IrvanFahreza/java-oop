package logic.logicInterface.logic02Impl;

import logic.BasicLogic;
import logic.logicInterface.LogicInterface;

public class Logic02Soal02Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic02Soal02Impl(BasicLogic logic) {
        this.logic = logic;
    }
    
    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {
            int angka = 1;
            int akhir = 17;
            for (int j = 0; j < this.logic.n; j++) {
                if(i<=j && i==0 || i>=j && i==8 ){
                    this.logic.array[i][j] = String.valueOf(angka);
                    angka+=2;
                } else if(i<=j && j==8){
                    this.logic.array[i][j] = String.valueOf(akhir);
                } else if(i>=j && j==0){
                    this.logic.array[i][j] = String.valueOf(angka);
                }
            }
        }
    }
    public void isiArray2(){
        for (int i = 0; i < this.logic.n; i++) {
            int x = 1;
            int y = 17;
            for (int j = 0; j < this.logic.n; j++) {
                if(i<=j && i==0 || i>=j && i==8 ){
                    this.logic.array[i][j] = String.valueOf(x);
                    x+=2;
                } else if(i<=j && j==8){
                    this.logic.array[i][j] = String.valueOf(y);
                } else if(i>=j && j==0){
                    this.logic.array[i][j] = String.valueOf(x);
                }
            }
        }
    }
    public void isiArray3(){
        for (int i = 0; i < this.logic.n; i++) {
            int a = 1;
            int b = 17;
            for (int j = 0; j < this.logic.n; j++) {
                if(i<=j && i==0 || i>=j && i==8 ){
                    this.logic.array[i][j] = String.valueOf(a);
                    a+=2;
                } else if(i<=j && j==8){
                    this.logic.array[i][j] = String.valueOf(b);
                } else if(i>=j && j==0){
                    this.logic.array[i][j] = String.valueOf(a);
                }
            }
        }
    }
    public void isiArray4(){
        for (int i = 0; i < this.logic.n; i++) {
            int l = 1;
            int m = 17;
            for (int j = 0; j < this.logic.n; j++) {
                if(i<=j && i==0 || i>=j && i==8 ){
                    this.logic.array[i][j] = String.valueOf(l);
                    l+=2;
                } else if(i<=j && j==8){
                    this.logic.array[i][j] = String.valueOf(m);
                } else if(i>=j && j==0){
                    this.logic.array[i][j] = String.valueOf(l);
                }
            }
        }
    }
    public void isiArray5(){
        for (int i = 0; i < this.logic.n; i++) {
            int number = 1;
            int ends = 17;
            for (int j = 0; j < this.logic.n; j++) {
                if(i<=j && i==0 || i>=j && i==8 ){
                    this.logic.array[i][j] = String.valueOf(number);
                    number+=2;
                } else if(i<=j && j==8){
                    this.logic.array[i][j] = String.valueOf(ends);
                } else if(i>=j && j==0){
                    this.logic.array[i][j] = String.valueOf(number);
                }
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.print();
    }
    public void cetakArray2() {
        this.isiArray2();
        this.logic.print();
    }
    public void cetakArray3() {
        this.isiArray3();
        this.logic.print();
    }
    public void cetakArray4() {
        this.isiArray4();
        this.logic.print();
    }
    public void cetakArray5() {
        this.isiArray5();
        this.logic.print();
    }
}
