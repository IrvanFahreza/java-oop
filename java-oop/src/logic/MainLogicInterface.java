package logic;

import logic.logicInterface.LogicInterface;
import logic.logicInterface.logic01Impl.*;
import logic.logicInterface.logic02Impl.*;


public class MainLogicInterface {
    public static void main(String[] args) {
        System.out.println("====================== LOGIC 01 =============================");
        System.out.println("============ LOGIC 01 SOAL 01 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal01 = new Logic01Soal01Impl(new BasicLogic(9));
        logic01Soal01.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal01Impl) logic01Soal01).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal01Impl) logic01Soal01).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal01Impl) logic01Soal01).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal01Impl) logic01Soal01).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 02 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal02 = new Logic01Soal02Impl(new BasicLogic(9));
        logic01Soal02.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal02Impl) logic01Soal02).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal02Impl) logic01Soal02).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal02Impl) logic01Soal02).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal02Impl) logic01Soal02).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 03 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal03 = new Logic01Soal03Impl(new BasicLogic(9));
        logic01Soal03.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal03Impl) logic01Soal03).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal03Impl) logic01Soal03).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal03Impl) logic01Soal03).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal03Impl) logic01Soal03).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 04 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal04 = new Logic01Soal04Impl(new BasicLogic(9));
        logic01Soal04.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal04Impl) logic01Soal04).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal04Impl) logic01Soal04).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal04Impl) logic01Soal04).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal04Impl) logic01Soal04).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 05 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal05 = new Logic01Soal05Impl(new BasicLogic(9));
        logic01Soal05.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal05Impl) logic01Soal05).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal05Impl) logic01Soal05).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal05Impl) logic01Soal05).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal05Impl) logic01Soal05).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 07 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal07 = new Logic01Soal07Impl(new BasicLogic(9));
        logic01Soal07.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal07Impl) logic01Soal07).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal07Impl) logic01Soal07).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal07Impl) logic01Soal07).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal07Impl) logic01Soal07).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 08 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal08 = new Logic01Soal08Impl(new BasicLogic(9));
        logic01Soal08.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal08Impl) logic01Soal08).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal08Impl) logic01Soal08).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal08Impl) logic01Soal08).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal08Impl) logic01Soal08).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 09 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal09 = new Logic01Soal09Impl(new BasicLogic(9));
        logic01Soal09.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal09Impl) logic01Soal09).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal09Impl) logic01Soal09).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal09Impl) logic01Soal09).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal09Impl) logic01Soal09).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 01 SOAL 10 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic01Soal10 = new Logic01Soal10Impl(new BasicLogic(9));
        logic01Soal10.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic01Soal10Impl) logic01Soal10).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic01Soal10Impl) logic01Soal10).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic01Soal10Impl) logic01Soal10).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic01Soal10Impl) logic01Soal10).cetakArray5();
        System.out.println("\n");

        System.out.println("====================== LOGIC 02 =============================");
        System.out.println("============ LOGIC 02 SOAL 01 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic02Soal01 = new Logic02Soal01Impl(new BasicLogic(9));
        logic02Soal01.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic02Soal01Impl) logic02Soal01).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic02Soal01Impl) logic02Soal01).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic02Soal01Impl) logic02Soal01).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic02Soal01Impl) logic02Soal01).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 02 SOAL 02 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic02Soal02 = new Logic02Soal02Impl(new BasicLogic(9));
        logic02Soal02.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic02Soal02Impl) logic02Soal02).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic02Soal02Impl) logic02Soal02).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic02Soal02Impl) logic02Soal02).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic02Soal02Impl) logic02Soal02).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 02 SOAL 03 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic02Soal03 = new Logic02Soal03Impl(new BasicLogic(9));
        logic02Soal03.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic02Soal03Impl) logic02Soal03).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic02Soal03Impl) logic02Soal03).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic02Soal03Impl) logic02Soal03).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic02Soal03Impl) logic02Soal03).cetakArray5();
        System.out.println("\n");

        System.out.println("============ LOGIC 02 SOAL 04 ==============");
        System.out.println("1x Coba:");
        LogicInterface logic02Soal04 = new Logic02Soal04Impl(new BasicLogic(9));
        logic02Soal04.cetakArray();
        System.out.println("\n2x Coba:");
        ((Logic02Soal04Impl) logic02Soal04).cetakArray2();
        System.out.println("\n3x Coba:");
        ((Logic02Soal04Impl) logic02Soal04).cetakArray3();
        System.out.println("\n4x Coba:");
        ((Logic02Soal04Impl) logic02Soal04).cetakArray4();
        System.out.println("\n5x Coba:");
        ((Logic02Soal04Impl) logic02Soal04).cetakArray5();
        System.out.println("\n");

//        System.out.println("\n\nLogic 02 Soal 09");
//        LogicInterface logic02Soal09 = new Logic02Soal09Impl(new BasicLogic(9));
//        logic02Soal09.cetakArray();
    }
}
