package numberClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MainScanner {
    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

        System.out.println("Masukkan Nama: ");
        String nama = scanner.nextLine();

        System.out.println("Masukkan tanggal lahir: ");
        String tglLahir = scanner.nextLine();
        Date tglLahirDate = formater.parse(tglLahir);
        System.out.println("tglLahirDate = " + tglLahirDate);

        Date currentDate = new Date();
        String tglSekarang = formater.format(currentDate);
        System.out.println("tgl Sekarang date: " + currentDate);
        System.out.println("tgl Sekarang: " + tglSekarang);

        String tahunLahir = formatYear.format(tglLahirDate);
        System.out.println("Tahun: " + tahunLahir);
        String tahunSekarang = formatYear.format(currentDate);
        System.out.println("Tahun Skrng: " + tahunSekarang);

        // hitung umur
        Calendar calendar = Calendar.getInstance();
        // tgl date => calender
        calendar.setTime(tglLahirDate);
        System.out.println("calender tgl lahir "+ calendar.getTime());
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(new Date());
        // check calender sekarang
        System.out.println("tgl Sekarang "+ currentCalendar.getTime());
        // ambil selesih
        Long time = (currentCalendar.getTimeInMillis() - calendar.getTimeInMillis())/1000;
        System.out.println("millisecond = " + time);
        // second => minute
        Long minute = (time/60);
        System.out.println("minute = " + minute);
        // minute => hour
        Long hour = (minute/60);
        System.out.println("hour = " + hour);
        // hour => day
        Long day = (hour/24);
        System.out.println("day = " + day);
        // day => week
        Long week = (day/7);
        System.out.println("week = " + week);
        // day => month
        Long month = (day/30);
        System.out.println("month = " + month);
        // day => year
        Long year = (day/365);
        System.out.println("Umur = "+ year);
    }
}
