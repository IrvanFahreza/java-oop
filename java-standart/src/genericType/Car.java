package genericType;

import java.util.StringJoiner;

public class Car {
    private String name;
    private Double price;
    private String tier;

    public String getName() {
        return name;
    }

    public Car(String name, Double price, String tier) {
        this.name = name;
        this.price = price;
        this.tier = tier;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("price=" + price)
                .add("tier='" + tier + "'")
                .toString();
    }
}
