package genericType;

public class MainDataGeneric {
    public static void main(String[] args) {
        DataGeneric<String> data1 = new DataGeneric<String>();
        data1.setData("Irvan Fahreza");
        System.out.println("data1 Value "+ data1.getData());

        //data1.setData(1234567); => error

        DataGeneric<Integer> data2 = new DataGeneric<Integer>();
        data2.setData(1234567);
        System.out.println("data2 Value "+ data2.getData());

        DataGeneric<Boolean> data3 = new DataGeneric<Boolean>();
        data3.setData(true);
        System.out.println("data2 Value "+ data3.getData());

        DataGeneric<Product> data4 = new DataGeneric<Product>();
        //data4.setData(new Product("Ajinomoto",2500.00));
        Product product = new Product("Obat Awet Muda",1000000.0);
        data4.setData(product);
        System.out.println("data4 Value "+ data4.getData());

        DataGeneric<Person> data5 = new DataGeneric<Person>();
        Person person = new Person("Irvan", "Jakarta Selatan",23);
        data5.setData(person);
        System.out.println("data 5 = " + data5.getData());

        DataGeneric<Animal> data6 = new DataGeneric<Animal>();
        Animal animal = new Animal("Kuda",4);
        data6.setData(animal);
        System.out.println("data6 = " + data6.getData());

        DataGeneric<Car> data7 = new DataGeneric<Car>();
        Car car = new Car("Lambo", 2000000000.0,"S");
        data7.setData(car);
        System.out.println("data7 = " + data7.getData());

        DataGeneric<Sports> data8 = new DataGeneric<Sports>();
        Sports sports = new Sports("Football", "Playing football");
        data8.setData(sports);
        System.out.println("data8 = " + data8.getData());

        DataGeneric<Food> data9 = new DataGeneric<Food>();
        Food food = new Food("Bakso",25000.0);
        data9.setData(food);
        System.out.println("data9 = " + data9.getData());

        DataGeneric<Laptop> data10 = new DataGeneric<Laptop>();
        Laptop laptop = new Laptop("Acer", 10000000.0);
        data10.setData(laptop);
        System.out.println("data10 = " + data10.getData());
    }
}
