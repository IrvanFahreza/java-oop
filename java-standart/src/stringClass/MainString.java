package stringClass;

public class MainString {
    public static void main(String[] args) {
        String name = "Irvan Reza";
        System.out.println("To Lower: "+name.toLowerCase());
        System.out.println("To Upper: "+name.toUpperCase());
        System.out.println("Length : "+name.length());
        System.out.println("Start with: "+name.startsWith("Van"));
        System.out.println("Ends with: "+name.endsWith("van"));

        String[] names = name.split(" ");
        System.out.println("Jumlah kata: "+ names.length);
        System.out.println("Index ke 0: "+ names[0]);
        System.out.println("Index ke 1: "+ names[1]);

    }

}
