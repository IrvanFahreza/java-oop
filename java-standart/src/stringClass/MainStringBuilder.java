package stringClass;

public class MainStringBuilder {
    public static void main(String[] args) {
        String firstName = "Irvan ";
        System.out.println("firstName = " + firstName);
        String lastName = "Fahreza";
        System.out.println("lastName = " + lastName);

        String nameLengkap = firstName+" "+lastName;
        System.out.println("nameLengkap = " + nameLengkap);

        StringBuilder builder = new StringBuilder(firstName).
                append(" ").append(lastName);
        System.out.println("Builder "+builder);

    }
}
