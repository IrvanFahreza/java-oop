package stringClass;

import java.util.StringJoiner;

public class MainStringJoiner {
    public static void main(String[] args) {
        String[] names = {"Irvan","Fahreza","Fatima"};
        StringJoiner joiner = new StringJoiner(
                "-", "[", "]"
        );

        System.out.println("Sebelum di add: ");
        System.out.println(joiner.toString());
        for(String name:names){
            joiner.add(name);
        }

        System.out.println("Sesudah di add: ");
        System.out.println(joiner.toString());
    }
}
