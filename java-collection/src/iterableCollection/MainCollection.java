package iterableCollection;

import model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MainCollection {
    public static void main(String[] args) {
        Collection<String> names = new ArrayList<>();
        names.add("Irvan");
        names.add("Fatima");
        names.addAll(List.of("Reza", "Fahreza", "Ima", "Reja", "Rehja", "Rehji"));

        System.out.println(names.contains("Rezi"));
        System.out.println(names.containsAll(Arrays.asList("Reja")));

        names.remove("Reja");
        names.removeAll(Arrays.asList("Rehji","Rehja"));

        System.out.println(names.contains("Rezi"));
        System.out.println(names.containsAll(Arrays.asList("Reja")));

        // loop data
        for (String name: names){
            System.out.println(name);
        }

        Collection<Person> persons = new ArrayList<>();
        persons.add(new Person(1,"Irvan","Palembang"));
        persons.add(new Person(2,"Reza","Jakarta"));
        persons.add(new Person(3,"Fatima","Bandung"));
        // .asList = Immutable (isinya tidak bisa dirubah/edit)
        persons.addAll( Arrays.asList(new Person(4,"Tertia","Bandung")));
        persons.addAll( Arrays.asList(
                new Person(5,"Rizky","Lampung"),
                new Person(6,"Agung","Semarang")
        ));


        for (Person p: persons){
            System.out.println(p);
        }
    }
}
