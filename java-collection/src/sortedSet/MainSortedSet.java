package sortedSet;

import model.Person;

import java.util.SortedSet;
import java.util.TreeSet;

public class MainSortedSet {
    public static void main(String[] args) {
        SortedSet<Person> persons = new TreeSet<>(new PersonComparator());
        persons.add(new Person(1, "Irvan","Palembang"));
        persons.add(new Person(2, "Irvan1","Palembang1"));
        persons.add(new Person(3, "Irvan2","Palembang2"));
        persons.add(new Person(4, "Irvan3","Palembang3"));
        persons.add(new Person(5, "Irvan4","Palembang4"));
        persons.add(new Person(6, "Irvan5","Palembang5"));

        for (Person p: persons){
            System.out.println(p);
        }
    }
}
