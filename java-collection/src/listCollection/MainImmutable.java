package listCollection;

import model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainImmutable {
    public static void main(String[] args) {
        Person person = new Person("Irvan",
                Arrays.asList("Coding","Reading","Sports"));
        System.out.println(person);

        // cara salah
        // person.getHobbies().add("Gaming"):

        // cara benar
        List<String> hobbies = new ArrayList<>(person.getHobbies());
        hobbies.add("Gaming");
        person.setHobbies(hobbies);
        System.out.println(person);
    }
}
